# CHANGELOG



## v0.2.1 (2024-04-07)

### Fix

* fix(deploy): add deploy pipeline ([`3bfc3da`](https://gitlab.com/mm-farina/my-package2/-/commit/3bfc3da6df0aa3b8ded7a3ec9130667ab89ab53e))


## v0.2.0 (2024-04-07)

### Feature

* feat(addition): add addition feature ([`c85acc3`](https://gitlab.com/mm-farina/my-package2/-/commit/c85acc3d501b6da0d6c893a0fc705f7b020aa809))


## v0.1.0 (2024-04-07)

### Feature

* feat(semantic-release): add semantic release to the repository ([`a10ffcd`](https://gitlab.com/mm-farina/my-package2/-/commit/a10ffcd879b18e4813f1d246fe1283826dd044a7))

### Unknown

* Add semantic release config ([`91a3484`](https://gitlab.com/mm-farina/my-package2/-/commit/91a3484389bbb8aae19053008270786e22ccc591))

* Add pyproject.toml to init poetry ([`64bbf3a`](https://gitlab.com/mm-farina/my-package2/-/commit/64bbf3ade123a61f59444736d186c25cc6935a98))

* Update .gitlab-ci.yml file ([`f97fc4f`](https://gitlab.com/mm-farina/my-package2/-/commit/f97fc4f70017b1614af053709c63457149d9be9a))

* Add first pipeline ([`ec00fe7`](https://gitlab.com/mm-farina/my-package2/-/commit/ec00fe7a03faa47d4786f4399ee379e5dbd4e8e1))

* Initial commit ([`2ffd053`](https://gitlab.com/mm-farina/my-package2/-/commit/2ffd05313931e377ad08832cc69a143e10dd7ac1))
